# projet-m1-miage-2021-team-M

API de gestion de petites annonces (type Leboncoin)
L'utilisateur pourra se connecter, mettre une annonce en ligne, la gérer par la suite (modification/suppression) et consulter toutes les annonces.

## Installation

Suivez les instructions pour installer les packages/modules.

Prérequis : node.JS v14.16.0

```bash
git clone https://gitlab.com/elgamalmahmoud.pro/projet-m1-miage-2021-team-m.git

npm install

node bin/server
```

## Utilisation

Pour s'inscrire (POST) https://localhost:3000/api/signup

```bash
{
   "username" : "VotreUsername"
   "password" : "VotreMotDePasse"
}
```

Pour se connecter (POST) https://localhost:3000/api/signin

Pour JWT, il faut récupérer le token, et pour basic authentification :

```bash
{
   "username" : "VotreUsername"
   "password" : "VotreMotDePasse"
}
```

Pour créer une annonce (POST) https://localhost:3000/api/annonce


```bash
{
        "title": "Titre",
        "description": "Description",
        "imageUrl": "imageurl",
        "price": "XXX",
}
```

Pour voir toutes les annonces (GET) https://localhost:3000/api/annonce


```bash
Renvoie toutes les annonces en format JSON
```

Pour voir une annonce selon son ID (GET) https://localhost:3000/api/annonce/:id


```bash
Renvoie une annonce selon son ID si elle existe en format JSON
```

Pour modifier une annonce selon son ID (PUT) https://localhost:3000/api/annonce/:id


```bash
{
        "title": "Nouveau Titre",
        "description": "Nouvelle Description",
        "imageUrl": "Nouvelle imageurl",
        "price": "Nouveau prix XXX",
}
```



Pour supprimer une annonce selon son ID (DELETE) https://localhost:3000/api/annonce/:id


```bash
Supprime une annonce selon son ID si elle existe en format JSON
```

## Auteurs

Younès SAHA et Mahmoud ELGAMAL.