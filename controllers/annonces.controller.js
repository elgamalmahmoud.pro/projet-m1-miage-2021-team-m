const Annonce = require('../models/Annonce');


exports.createAnnonce = (req, res, next) => {
    const annonce = new Annonce({
        title: req.body.title,
        description: req.body.description,
        imageUrl: req.body.imageUrl,
        userId: req.user._id,
        price: req.body.price,
    });
    annonce.save().then(
        () => {
            res.status(201).json({
                message: 'Annonce créée !'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.getAllAnnonces = (req, res, next) => {

   // let token = getToken(req.headers);
    //if (token) {
        Annonce.find(function (err, annonces) {
            if (err) return next(err);
            res.json(annonces);
        });

     //   return res.status(403).send({success: false, msg: 'Unauthorized.'});
   // }


}

exports.getOneAnnonce = (req, res, next) => {
    Annonce.findOne({ _id: req.params.id })
        .then(annonce => res.status(200).json(annonce))
        .catch(error => res.status(404).json({ error }));
}

exports.modifyAnnonce = (req, res, next) => {
    Annonce.updateOne({ _id: req.params.id }, { ...req.body, _id: req.params.id })
        .then(() => res.status(200).json({ message: 'Annonce modifiée !'}))
        .catch(error => res.status(400).json({ error }));
}

exports.deleteAnnonce = (req, res, next) => {
    Annonce.deleteOne({ _id: req.params.id })
        .then(() => res.status(200).json({ message: 'Annonce supprimée !'}))
        .catch(error => res.status(400).json({ error }));
}

/*
getToken = function (headers) {
    if (headers && headers.authorization) {
        let parted = headers.authorization.split(' ');
        if (parted.length === 2) {
            return parted[1];
        } else {
            return null;
        }
    } else {
        return null;
    }
};

 */