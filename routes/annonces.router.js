const express = require('express');

const router = express.Router();
const annonceController = require('../controllers/annonces.controller');


router.get('/annonce', annonceController.getAllAnnonces);
router.post('/annonce', annonceController.createAnnonce);
router.get('/annonce/:id', annonceController.getOneAnnonce);
router.put('/annonce/:id', annonceController.modifyAnnonce);
router.delete('/annonce/:id', annonceController.deleteAnnonce);



module.exports = router;