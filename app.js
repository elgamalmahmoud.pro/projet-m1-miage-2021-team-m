const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const bodyParser = require('body-parser');

require('./middleware/auth')(passport);



const app = express();

mongoose.connect('mongodb+srv://miage95:projetmiage@cluster0.7hzp3.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
  { useNewUrlParser: true,
    useUnifiedTopology: true, useCreateIndex: true  })
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch(() => console.log('Connexion à MongoDB échouée !'));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(passport.initialize());

const userRoutes = require('./routes/user.router');
const annoncesRoutes = require('./routes/annonces.router');

app.use('/api', userRoutes);
app.use('/api', passport.authenticate(['jwt','basic'], { session: false}), annoncesRoutes);

module.exports = app;