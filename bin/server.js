const http = require('http');
const app = require('../app');
const pem = require ('pem');


const fs = require('fs');
const https = require('https');


const normalizePort = val => {
    const port = parseInt(val, 10);

    if (isNaN(port)) {
        return val;
    }
    if (port >= 0) {
        return port;
    }
    return false;
};
const port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

const errorHandler = error => {
    if (error.syscall !== 'listen') {
        throw error;
    }
    const address = server.address();
    const bind = typeof address === 'string' ? 'pipe ' + address : 'port: ' + port;
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requiert des privilèges élevés.');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' est déjà uutilisé.');
            process.exit(1);
            break;
        default:
            throw error;
    }
};


const server = https.createServer({
    key: fs.readFileSync('./ssl/localhost+2-key.pem'),
    cert: fs.readFileSync('./ssl/localhost+2.pem'),
    requestCert: false,
    rejectUnauthorized: false
}, app);

/*
const { Server } = require("socket.io");
const ios = new Server(server);


ios.on('connection', (socket) => {
    console.log("Connected succesfully to the socket ...");
});

*/


server.on('error', errorHandler);
server.on('listening', () => {
    const address = server.address();
    const bind = typeof address === 'string' ? 'pipe ' + address : 'port ' + port;
    console.log('Listening on ' + bind);
});

server.listen(port);



