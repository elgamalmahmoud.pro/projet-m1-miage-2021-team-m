const User = require('../models/User');

const jwt = require('jsonwebtoken');


exports.signup = (req, res, next) => {

    if (!req.body.username || !req.body.password) {
        res.json({success: false, msg: 'Entrez un pseudo et un mot de passe.'});
    } else {
        const newUser = new User({
            username: req.body.username,
            password: req.body.password
        });
        // save the user
        newUser.save(function(err) {
            if (err) {
                return res.json({success: false, msg: 'Ce pseudo existe déjà.'});
            }
            res.json({success: true, msg: 'Bravo, votre compte est créé.'});
        });
    }

};

exports.signin = (req, res, next) => {



    User.findOne({
        username: req.body.username
    }, function(err, user) {
        if (err) throw err;

        if (!user) {
            res.status(401).send({success: false, msg: 'Echec de la connexion. Ce pseudo n existe pas.'});
        } else {
            // check if password matches

                if (req.body.password === user.password) {
                    // if user is found and password is right create a token
                    const token = jwt.sign(user.toJSON(), "miage");
                    // return the information including token as JSON
                    res.json({success: true, token: 'JWT ' + token});
                } else {
                    res.status(401).send({success: false, msg: 'Echec de la connexion. Mauvais mot de passe.'});
                }

        }
    });
};
